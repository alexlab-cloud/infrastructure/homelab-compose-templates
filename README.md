# infra.sys.container-stacks

> *Compose stack templates for self-hosting apps on workstations and servers*

## Usage

### Compose Files

***Prerequisites:*** [Docker and Docker Compose](https://docs.docker.com/engine/install/ubuntu/), or alternatively
[Podman][href.podman] and [podman-compose][href.podman-compose].

[Compose files][href.compose-spec] are organized under their respective app folders in [`src/stacks/`](src/stacks/).
Any one compose file contains one or more containerized service definitions with some standardized features, e.g.
variable names and volume structuring. When launched, ephemeral services will start and expose their applications on
localhost/the local network. Some of these stacks may be better suited to either a server or workstation environment,
but don't let that stop you from launching them wherever you need them to run.

### Starting Containers: Simple

Each [template](./templates/) can be easily run with `docker compose -f ${COMPOSE_FILE} up`, e.g.
`docker compose -f src/stacks/network/nginx-proxy-manager/docker-compose.nginx-proxy-manager.yml`. 
Some environment variables may need to be replaced depending on the application.

### Managing Containers: Delivery Platform

It can be helpful to use a [service delivery platform][href.serv-deliv-plat] to manage container stacks, especially
when hosting multiple services for a long period of time. Managing containers via the CLI is perfectly useful, and even
preferable sometimes depending on available system resources, but applications like [Portainer][href.portainer-docs]
and [Yacht][href.yacht-docs] provide great UIs and automation endpoints that make it easier to look after fleets of
services running on one or more machines.

The provided [Portainer Server stack](./src/stacks/utility/portainer/docker-compose.portainer-server.yml) can be started
first and used as a management application for the other containers. The [`portainer-bootstrap.bash`](./portainer-bootstrap.bash)
script is designed to start [`portainer-ce-without-annoying`][href.portainer-ce-without-annoying] on any machine with
`docker` and `docker-compose` installed

## Resources

1. [docs.docker.com: *Compose variable interpolation*][1]
2. [Awesome-Selfhosted][2]

[1]: https://docs.docker.com/compose/environment-variables/variable-interpolation
[2]: https://awesome-selfhosted.net/

<!-- Links -->
[href.docker]: https://www.docker.com/
[href.podman]: https://podman.io/
[href.podman-compose]: https://github.com/containers/podman-compose#installation
[href.compose-spec]: https://github.com/compose-spec/compose-spec/blob/main/spec.md
[href.serv-deliv-plat]: https://en.wikipedia.org/wiki/Service_delivery_platform
[href.portainer-docs]: https://docs.portainer.io
[href.yacht-docs]: https://yacht.sh/
[href.portainer-ce-without-annoying]: https://github.com/ngxson/portainer-ce-without-annoying
