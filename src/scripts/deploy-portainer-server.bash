#!/usr/bin/env bash

# Bootstrap the executing machine with Portainer to manage other containerized services. Portainer's initial
# configuration will reference this repository for GitOps-style management.

COMPOSE_FILE_URL="https://gitlab.com/alexlab-cloud/infra/sys-lab/container-stacks/-/raw/main/src/stacks/utility/portainer/docker-compose.portainer-server.yml?ref_type=heads"
INSTALL_DIR="${HOME}/.tmp"
PORTAINER_COMPOSE_FILENAME="docker-compose.portainer-server.yml"

PORT="${PORTAINER_PORT:-8888}"
DATA_ROOT="${CONTAINER_DATA_ROOT:-$HOME/.local/share/container-data}"
CONFIG_ROOT="${CONTAINER_CONFIG_ROOT:-$HOME/container-configs}"

# Make sure the data directory for containers exists on the current system, otherwise do nothing
mkdir -p "${DATA_ROOT}" "${CONFIG_ROOT}"

if curl -L -o "${INSTALL_DIR}/${PORTAINER_COMPOSE_FILENAME}" "${COMPOSE_FILE_URL}"; then
  echo "Compose file downloaded successfully; starting..."
  PORT="${PORT}" DATA_ROOT=$ docker compose -d -f "${PORTAINER_COMPOSE_PATH}" up
fi
